public with sharing class CreateNewCaseComment {
   

   public static void execute(){

       List<CaseComment> lLstCaseComment = new List<CaseComment>();
       for(Case iCase : (List<Case>) Trigger.new){
           
           if(iCase.Status == 'Escalated' && iCase.Priority == 'Medium'){
               lLstCaseComment.add(new CaseComment(CommentBody= 'Comentário do caso criado com sucesso', ParentId= iCase.Id));
           }
       }

       if(!lLstCaseComment.isEmpty()){
           Database.insert(lLstCaseComment);
       }
   }
}