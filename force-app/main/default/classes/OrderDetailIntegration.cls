public with sharing class OrderDetailIntegration {
    

    @AuraEnabled(Cacheable = true)
    public static String getOrderDetails(){
  
        SwagOrder lOrder =  SwagOrder.getExample();
         
        return JSON.serializePretty(lOrder);     
    }
}