public with sharing class UpdateOpportunityStage {
    

    public static void execute(){
        List<Opportunity> lLstOpp = new List<Opportunity>();
        for(Opportunity iOpportunity : (List<Opportunity>) Trigger.new){
            if(iOpportunity.Type == 'New Customer' &&
               String.isNotBlank(iOpportunity.AccountId)){
                lLstOpp.add(iOpportunity);
            } 
        }

        if(lLstOpp.isEmpty()){ return ;}
        System.debug('THBS --- lMaplLstAccountOpportunityId' + lLstOpp);
        updateStage(lLstOpp); 
    }

    private static void updateStage(List<Opportunity> aLstOpp){

        System.debug('THBS ----- aLstOpp' + aLstOpp);
        if(aLstOpp.isEmpty()){ return ;} 

        for(Opportunity iOpp : aLstOpp){
                System.debug('Caiu no for'); 
                iOpp.StageName = 'Qualification';   
            }
    }

}