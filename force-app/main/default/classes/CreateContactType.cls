public with sharing class CreateContactType {
    

    public static void execute(){ 

        List<Account> lLstAccount = new List<Account>();
        for(Account iAccount : (List<Account>) Trigger.new){
            if(iAccount.Type == 'Customer - Channel' &&
               iAccount.Ownership == 'Public'){
                   lLstAccount.add(iAccount);
            }
        }
        if(lLstAccount.isEmpty() || lLstAccount == null){ return ; }
        createContact(lLstAccount);

    }

    private static void createContact(List<Account> aLstAccount){
        
        if(aLstAccount.isEmpty() || aLstAccount == null){ return ;}
        
        List<Contact> lLstContact = new List<Contact>();
        for(Account iAccount : aLstAccount){
            Contact lContact = new Contact();
            lContact.AccountId = iAccount.Id;
            lContact.LastName = 'Thbs Souza';
            lContact.Title = Label.lContactDescription;
            lLstContact.add(lContact); 

        }

        if(!lLstContact.isEmpty()){
            Database.insert(lLstContact);
        }
    }
}