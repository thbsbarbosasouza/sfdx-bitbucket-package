public with sharing class CreateNewCaseInstalationPartner {


    public static void execute(){
        List<Case> lLstNewCases = new List<Case>();
        for(Account iAccount : (List<Account>) Trigger.new){
            if(iAccount.Type == 'Installation Partner'){
                lLstNewCases.add(new Case(AccountId = iAccount.Id, Status = 'New', Reason = 'Installation', Type= 'Electrical'));
            }
        }

        if(!lLstNewCases.isEmpty()){
            Database.insert(lLstNewCases);
        }
    }
}