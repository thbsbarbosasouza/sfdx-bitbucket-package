public with sharing class DetailController {
     

    @AuraEnabled(Cacheable = true)
    public static List<Account> getRecords(String recordId){


        List<Account> lLstAcc = [SELECT Name, Type, Industry, Region__c
                                    FROM Account WHERE Id =: recordId];  

        return lLstAcc;            
    
    }

    @AuraEnabled(Cacheable = true)
    public static List<Opportunity> getRecordOpportunity(String recordId){
        List<Opportunity> lLstOpportunity = [SELECT Id, ForecastCategoryName, Name, StageName, LeadSource
                                             FROM Opportunity 
                                             WHERE Id=: recordId];
        return lLstOpportunity; 
    }

    @AuraEnabled(Cacheable = true)
    public static List<Case> getObjectRecords(String recordId){
        List<Case> lLstCase = [SELECT Id, Reason, Subject, Type, Status,  CaseNumber FROM Case WHERE Opportunity__c =: recordId];
         
        return lLstCase;
    } 

}