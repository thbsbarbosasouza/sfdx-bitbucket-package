public with sharing class AccountDetailController {
    

    @AuraEnabled(Cacheable = true)
    public static List<Account> getRecords(String recordId){


        List<Account> lLstAcc = [SELECT Name, Type, Industry, Region__c
                                    FROM Account WHERE Id =: recordId];  

        return lLstAcc;            
    } 
}