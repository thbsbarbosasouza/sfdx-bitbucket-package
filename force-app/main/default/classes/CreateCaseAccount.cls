public with sharing class CreateCaseAccount {
    

    public static void execute(){
        List<Account> lLstAccount = new List<Account>();
        for(Account iAccount : (List<Account>) Trigger.new){
            if(iAccount.Industry == 'Technology'){
                lLstAccount.add(iAccount);
            }
        }

        updateOwnerCase(lLstAccount);
    }

    private static void updateOwnerCase(List<Account> aLstAccount){
        if(aLstAccount.isEmpty() || aLstAccount == null){
            return;
        } 

        List<Group> lLstGroup = [SELECT Id 
                                FROM Group 
                                WHERE  Type = 'Queue' 
                                AND NAME = 'Account Queue'];

        List<Case> lLstCase = new List<Case>();
        for(Case iCase: [SELECT Id, ContactId,  AccountId FROM Case 
                         WHERE AccountId =: aLstAccount]){
            if(String.isBlank(iCase.ContactId)){
            iCase.OwnerId = lLstGroup[0].Id;
            lLstCase.add(iCase);
            }
        }

        Database.update(lLstCase);

    }
}