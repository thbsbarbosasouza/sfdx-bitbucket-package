trigger Account on Account(before insert, before update, after insert, after update) 
{
    if (trigger.isBefore) 
    {
        if (Trigger.isUpdate) 
        { 
    
        }
    }
    if (trigger.isAfter) 
    {
        if (Trigger.isInsert) 
        {

        }
        if (Trigger.isUpdate) 
        {
            CreateNewCaseInstalationPartner.execute();
            CreateContactType.execute();
            CreateCaseAccount.execute(); 
        }
    }
}