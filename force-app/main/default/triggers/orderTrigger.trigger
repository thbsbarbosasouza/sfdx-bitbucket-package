trigger orderTrigger on Order(before insert, before update, after insert, after update) 
{
    if (trigger.isBefore) 
    {
        if (Trigger.isUpdate) 
        {
  			OrderItemUtility.addBonusBouquet(Trigger.new);
        }
    }
    if (trigger.isAfter) 
    {
        if (Trigger.isInsert) 
        {
        }
        if (Trigger.isUpdate) 
        {
           
        }
    }
}