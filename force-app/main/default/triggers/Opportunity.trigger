trigger Opportunity on Opportunity(before insert, before update, after insert, after update) 
{
    if (trigger.isBefore) 
    {
        if(Trigger.isInsert){
            UpdateOpportunityStage.execute();  
        }
        
        if (Trigger.isUpdate) 
        {
        
        }
    }
    if (trigger.isAfter) 
    {
        if (Trigger.isInsert) 
        {

        }
        if (Trigger.isUpdate) 
        {
             
        }
    }
}