import { LightningElement, track, wire, api } from 'lwc';
import getRecords from '@salesforce/apex/OrderDetailIntegration.getOrderDetails';


const columns = [  
    { label: 'Status', fieldName: 'status', type: 'Text', sortable: true },
    { label: 'Ship Date', fieldName: 'shipDate', type: 'Text', sortable: true },
    {label: 'Quantity', fieldName: 'quantity', type: "Text", sortable: true},
    { label: 'Pet Id', fieldName: 'petId', type: "Text", sortable: true},
    { label: 'Id', fieldName: 'id', type: 'Text', sortable: true },      
    
    
    
];         

export default class orderDetailCustomize extends LightningElement{
        @api recordId;   
        @api strTitle;   
        @track columns;
        @track orders; 
        constructor() {
            super();
            this.strTitle='Account Contacts';
            this.columns = columns;
            this.orders = [];  
        }    
    
        // retrieving the data using wire service
        @wire(getRecords, {}) 
        records(result) {
            if (result.data) { 
                this.orders = [];
                result.data.forEach(item => {
                    let order = {};
                    order = { 
                        id:item.Id,
                        petId:item.petId,
                        quantity:item.quantity,
                        shipDate:item.shipDate,
                        status : item.status
                    };
                    this.orders.push(order);            
                });
            }
        }
}