import { LightningElement, track, wire, api } from 'lwc';
import getRecords from '@salesforce/apex/DetailController.getObjectRecords';
import {getRecord} from 'lightning/uiRecordApi';  
import {NavigationMixin} from 'lightning/navigation'; 

const actions = [
    { label: 'View', name: 'show_details' }, 
];  
const columns = [
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
        } 
    }, 
    { label: 'Case Number', fieldName: 'CaseNumber', type: 'Text', sortable: true },     
    { label: 'Type', fieldName: 'Type', type: "Text", sortable: true},
    {label: 'Reason', fieldName: 'Reason', type: "Text", sortable: true},
    { label: 'Status', fieldName: 'Status', type: 'Text', sortable: true },
];      

export default class detailsObject extends NavigationMixin(LightningElement) {
    @api recordId;  
    @track data = []; 
    @track columns = columns;
    @track tableLoadingState = true;
    @track rowOffset = 0;
    @track multiple = true;

    
    @track sortedBy;
    @track sortedDirection; 
    
    @wire(getRecords, {recordId: '$recordId'})
    
    
    
        wiredRecordsMethod({error, data}) {
            if (data) {
                this.data = data;
                this.error = undefined;
            } else if (error) {
                this.error = error;
                this.data = undefined;
            }
            this.tableLoadingState = false;
        }
    
        increaseRowOffset() {
            this.rowOffset += 1;
        }
    
        // The method onsort event handler
        updateColumnSorting(event) {
            let fieldName = event.detail.fieldName;
            let sortDirection = event.detail.sortDirection;
            // assign the latest attribute with the sorted column fieldName and sorted direction
            this.sortedBy = fieldName;
            this.sortedDirection = sortDirection;
            console.log('Sort fieldName: ' + fieldName);
            console.log('sort direction: ' + sortDirection);
    
            let reverse = sortDirection !== 'asc';
           
            let data_clone = JSON.parse(JSON.stringify(this.data));
    
            console.log('BEFORE data_clone:' + JSON.stringify(data_clone));
           
            this.data = data_clone.sort(this.sortBy(fieldName, reverse));
    
            console.log('AFTER data_clone:' + JSON.stringify(data_clone));
           
        }
    
        sortBy (field, reverse, primer){
    
            console.log('Sort by:reverse:' + reverse);
    
            var key = function (x) {return primer ? primer(x[field]) : x[field]};
         
            return function (a,b) {
                var A = key(a), B = key(b);
    
                if (A === undefined) A = '';
                if (B === undefined) B = '';
            
                return (A < B ? -1 : (A > B ? 1 : 0)) * [1,-1][+!!reverse];                  
            }
        } 

        navigateToRecordViewPage(event) {

 
            // View a custom object record.
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.row.Id,
                    objectApiName: 'Case', 
                    actionName: 'view'
                } 
            }); 
        }


        



    }