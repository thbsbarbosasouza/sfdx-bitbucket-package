import { LightningElement, wire, api, track } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class caseEditForm extends LightningElement {
    @track contactRecord = {}; 
    @api recordId;
    

    handleLoad(event) {
        if (!this.loadedForm) {
            let fields = Object.values(event.detail.records)[0].fields;
            const recordId = Object.keys(event.detail.records)[0];
            console.log('recordId:' + recordId);
            this.contactRecord = {
                Id: recordId,
                ...Object.keys(fields)
                    .filter((field) => !!this.template.querySelector(`[data-field=${field}]`))
                    .reduce((total, field) => {
                        total[field] = fields[field].value;
                        return total;
                    }, {})
            };
            this.loadedForm = true;
            this.recordId =recordId; 
        }
    } 
        handleFieldChange(e) {
            this.contactRecord[e.currentTarget.dataset.field] = e.target.value;
        }
        saveForm() {
            // if(this.validated())
            console.log('Contact for save => ', JSON.stringify(this.contactRecord));
            updateRecord({ fields: this.contactRecord })
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Contact updated',
                            variant: 'success'
                        })
                    );
                })
                .catch((error) => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error'
                        })
                    );
                });
        }
    }